#include "test_runner.h"

#include <iostream>
#include <map>
#include <string>
#include <unordered_map>

using namespace std;

struct Record {
	string id;
  	string title;
  	string user;
  	int timestamp;
  	int karma;

  	struct DatabaseIterators {
  	    multimap<int, Record*>::iterator timestamp_database_iterator;
  	    multimap<int, Record*>::iterator karma_database_iterator;
  	    multimap<string, Record*>::iterator user_database_iterator;
  	} databaseIterators;

  	bool operator==(const Record& other) {
  	    return tie(this->id, this->title, this->user, this->timestamp, this->karma) ==
  	           tie(other.id, other.title, other.user, other.timestamp, other.karma);
  	}
};

class Database {
public:
	bool Put(const Record& record);
	
  	const Record* GetById(const string& id) const;

  	bool Erase(const string& id);

  	template <typename Callback>
  	void RangeByTimestamp(int low, int high, Callback callback) const;

  	template <typename Callback>
  	void RangeByKarma(int low, int high, Callback callback) const;

  	template <typename Callback>
  	void AllByUser(const string& user, Callback callback) const;

private:
	unordered_map<string, Record> database;

  	multimap<int, Record*> timestamp_database;
	multimap<int, Record*> karma_database;
	multimap<string, Record*> user_database;

	template<typename Key, typename Callback>
	void RangeBy(const multimap<Key, Record*>& records_multimap,
                 const Key& low, const Key& high,
                 Callback callback) const;
};

bool Database::Put(const Record &record) {
    const auto [it, inserted] = database.insert({record.id, record});

    if (inserted) {
        const auto record_reference = addressof(it->second);
        it->second.databaseIterators = {
                timestamp_database.insert({record.timestamp, record_reference}),
                karma_database.insert({record.karma, record_reference}),
                user_database.insert({record.user, record_reference})
        };
    }

    return inserted;
}

const Record *Database::GetById(const string &id) const {
    const auto it = database.find(id);
    if (it != database.end()) {
        return addressof(it->second);
    }
    return nullptr;
}

bool Database::Erase(const string &id) {
    const auto it = database.find(id);
    if (it != database.end()) {
        const Record& record = it->second;
        timestamp_database.erase(record.databaseIterators.timestamp_database_iterator);
        karma_database.erase(record.databaseIterators.karma_database_iterator);
        user_database.erase(record.databaseIterators.user_database_iterator);

        database.erase(it);
        return true;
    }
    return false;
}

template<typename Key, typename Callback>
void Database::RangeBy(const multimap<Key, Record*>& records_multimap,
             const Key& low, const Key& high,
             Callback callback) const {
        if (low > high) { return; }
        const auto database_lower_bound = records_multimap.lower_bound(low);
        const auto database_upper_bound = records_multimap.upper_bound(high);
        for (auto it = database_lower_bound; it != database_upper_bound; ++it) {
            if (!callback(*it->second)) { break; }
        }
}

template<typename Callback>
void Database::RangeByTimestamp(int low, int high, Callback callback) const {
    RangeBy(timestamp_database, low, high, callback);
}

template<typename Callback>
void Database::RangeByKarma(int low, int high, Callback callback) const {
    RangeBy(karma_database, low, high, callback);
}

template<typename Callback>
void Database::AllByUser(const string &user, Callback callback) const {
    RangeBy(user_database, user, user, callback);
}

void TestRangeBoundaries() {
  const int good_karma = 1000;
  const int bad_karma = -10;

  Database db;
  db.Put({"id1", "Hello there", "master", 1536107260, good_karma});
  db.Put({"id2", "O>>-<", "general2", 1536107260, bad_karma});

  int count = 0;
  db.RangeByKarma(bad_karma, good_karma, [&count](const Record&) {
    ++count;
    return true;
  });

  ASSERT_EQUAL(2, count);
}

void TestSameUser() {
  Database db;
  db.Put({"id1", "Don't sell", "master", 1536107260, 1000});
  db.Put({"id2", "Rethink life", "master", 1536107260, 2000});

  int count = 0;
  db.AllByUser("master", [&count](const Record&) {
    ++count;
    return true;
  });

  ASSERT_EQUAL(2, count);
}

void TestReplacement() {
  const string final_body = "Feeling sad";

  Database db;
  db.Put({"id", "Have a hand", "not-master", 1536107260, 10});
  db.Erase("id");
  db.Put({"id", final_body, "not-master", 1536107260, -10});

  auto record = db.GetById("id");
  ASSERT(record != nullptr);
  ASSERT_EQUAL(final_body, record->title);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestRangeBoundaries);
  RUN_TEST(tr, TestSameUser);
  RUN_TEST(tr, TestReplacement);
  return 0;
}
