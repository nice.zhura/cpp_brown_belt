#include "ini.h"

#include <string_view>

pair<string_view, string_view> Split(string_view line, char by) {
    size_t pos = line.find(by);
    string_view left = line.substr(0, pos);

    if (pos < line.size() && pos + 1 < line.size()) {
        return {left, line.substr(pos + 1)};
    } else {
        return {left, string_view()};
    }
}

Ini::Section &Ini::Document::AddSection(string name) {
    return sections[move(name)];
}

const Ini::Section &Ini::Document::GetSection(const string &name) const {
    return sections.at(name);
}

size_t Ini::Document::SectionCount() const {
    return sections.size();
}

Ini::Document Ini::Load(istream &input) {
    Document result;
    Section* current_section;
    for(string line; getline(input, line);) {
        if (line.empty()) { continue; }
        if (line.front() == '[' && line.back() == ']') {
            string section_name = line.substr(1, line.size() - 2);
            current_section = &result.AddSection(move(section_name));
        } else {
            current_section->insert(Split(move(line), '='));
        }
    }
    return result;
}
