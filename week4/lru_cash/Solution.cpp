#include "Common.h"

#include <mutex>

#include <list>
#include <string_view>
#include <unordered_map>

using namespace std;

class LruCache : public ICache {
public:
  LruCache(
      shared_ptr<IBooksUnpacker> books_unpacker,
      const Settings& settings
  ) : current_memory_(0ull), max_memory_(settings.max_memory),
      books_unpacker_(move(books_unpacker))
  {
  }

  BookPtr GetBook(const string& book_name) override {
      lock_guard<mutex> lock_guard_(mutex_);

      BookPtr book;
      size_t book_memory_size;
      if (hash_map.find(book_name) == hash_map.end()) {
          book = books_unpacker_->UnpackBook(book_name);
          book_memory_size = book->GetContent().size();
          while (current_memory_ + book_memory_size > max_memory_) {
              if (deque.empty()) { return nullptr; }
              BookPtr last = deque.back();
              size_t last_memory_size = last->GetContent().size();

              deque.pop_back();
              hash_map.erase(last->GetName());
              current_memory_ -= last_memory_size;
          }
      } else {
          book = *hash_map.at(book_name);
          book_memory_size = book->GetContent().size();
          deque.erase(hash_map.at(book_name));
          current_memory_ -= book_memory_size;
      }

      deque.push_front(book);
      hash_map[book_name] = deque.begin();
      current_memory_ += book_memory_size;

      return deque.front();
  }

private:
    // lru cash structures
    list<BookPtr> deque;
    unordered_map<string, list<BookPtr>::iterator> hash_map;

    // size controllers
    size_t current_memory_;
    const size_t max_memory_;

    // books unpacker
    shared_ptr<IBooksUnpacker> books_unpacker_;

    // mutex
    mutable mutex mutex_;
};


unique_ptr<ICache> MakeCache(
    shared_ptr<IBooksUnpacker> books_unpacker,
    const ICache::Settings& settings
) {
    return make_unique<LruCache>(move(books_unpacker), settings);
}
