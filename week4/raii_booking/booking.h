#ifndef BOOKING_H
#define BOOKING_H

namespace RAII {

    template <typename Provider>
    class Booking {
    private:
        using BookingId = typename Provider::BookingId;

        Provider* provider;
        BookingId booking_id;

    public:
        Booking(Provider* p, const BookingId& id)
          : provider(p),
            booking_id(id)
        {
        }

        Booking(const Booking&) = delete;

        Booking(Booking&& other)
          : provider(other.provider),
            booking_id(other.booking_id)
        {
            other.provider = nullptr;
        }

        Booking& operator = (const Booking&) = delete;

        Booking& operator = (Booking&& other) {
            std::swap(provider, other.provider);
            std::swap(booking_id, other.booking_id);
            return *this;
        }

        ~Booking() {
            if (provider != nullptr) {
                provider->CancelOrComplete(*this);
            }
        }
    };
}

#endif // BOOKING_H
