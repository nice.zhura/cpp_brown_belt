#include "Common.h"
#include "test_runner.h"

#include <numeric>
#include <sstream>

using namespace std;

// Solution
// ---------------------------------------------------------------------------------

// Value Expression Class
// ---------------------------------------------------------------------------------

class ValueExpression : public Expression {
public:
   explicit ValueExpression(int value);
   int Evaluate() const override;
   string ToString() const override;
private:
    int value_;
};

ValueExpression::ValueExpression(int value)
: value_(value)
{
}

int ValueExpression::Evaluate() const {
    return value_;
}

string ValueExpression::ToString() const {
    return to_string(value_);
}

// ---------------------------------------------------------------------------------

// Binary Expression Class
// ---------------------------------------------------------------------------------

template<typename BinaryOperation, char op = 'o'>
class BinaryExpression : public Expression {
public:
    explicit BinaryExpression(ExpressionPtr left, ExpressionPtr right)
    : left_(move(left)), right_(move(right))
    {
    }

    int Evaluate() const override {
        return binary_operation_(left_->Evaluate(), right_->Evaluate());
    }

    string ToString() const override {
        string left_expression = "(" + left_->ToString() + ")";
        string right_expression = "(" + right_->ToString() + ")";
        return move(left_expression) + op + move(right_expression);
    }
private:
    BinaryOperation binary_operation_;
    ExpressionPtr left_;
    ExpressionPtr right_;
};

using SumExpression = BinaryExpression<plus<>, '+'>;
using ProductExpression = BinaryExpression<multiplies<>, '*'>;

// ---------------------------------------------------------------------------------

// Функции для формирования выражения
// ---------------------------------------------------------------------------------

ExpressionPtr Value(int value) {
    return make_unique<ValueExpression>(value);
}

ExpressionPtr Sum(ExpressionPtr left, ExpressionPtr right) {
    return make_unique<SumExpression>(move(left), move(right));
}

ExpressionPtr Product(ExpressionPtr left, ExpressionPtr right) {
    return make_unique<ProductExpression>(move(left), move(right));
}

// ---------------------------------------------------------------------------------


string Print(const Expression* e) {
  if (!e) {
    return "Null expression provided";
  }
  stringstream output;
  output << e->ToString() << " = " << e->Evaluate();
  return output.str();
}

void Test() {
  ExpressionPtr e1 = Product(Value(2), Sum(Value(3), Value(4)));
  ASSERT_EQUAL(Print(e1.get()), "(2)*((3)+(4)) = 14");

  ExpressionPtr e2 = Sum(move(e1), Value(5));
  ASSERT_EQUAL(Print(e2.get()), "((2)*((3)+(4)))+(5) = 19");

  ASSERT_EQUAL(Print(e1.get()), "Null expression provided");
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, Test);
  return 0;
}